package com.example.alunos.mudanadetela;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Alunos on 07/03/2018.
 */

public class Activity2 extends AppCompatActivity {
    TextView activity2;
    Button btn2;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        activity2 = (TextView)findViewById(R.id.textView);
        btn2 = (Button)findViewById(R.id.button4);
    }
    public void btn2ClickAct2(View v){
        Intent it = new Intent(this, MainActivity.class);
        startActivity(it);
    }
}
